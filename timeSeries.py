#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov 17 13:56:42 2021

@author: diogo
"""

from abc import ABC, abstractmethod
from os import path, remove
import json
import vtk
import numpy as np

class timeSeriesReader(ABC):

    def SetFileName(self,filename):
        self.filename = filename
        self.dirpath  = path.dirname( filename )
    
    def __init__(self):
         self.tmin = None
         self.tmax = None
         return None    

    def SetTimeInterval(self, tmin, tmax):
        self.tmin = tmin
        self.tmax = tmax
    
    @abstractmethod
    def ChangeTime(self,time):
        pass

    @abstractmethod
    def Update(self):
        pass        
        
    def GetTimeValues(self):
        return self.timevalues

    def GetReader(self):
        return self.reader
    
    @abstractmethod
    def GetDataSet(self):
        pass

class timeSeriesJSONReader(timeSeriesReader):        
        
    def Update(self):

        with open( self.filename) as f: 
             data = json.load(f)['files']
        
        self.timevalues = np.array( [ v["time"] for v in data ] )
        self.filelist  = np.array( [ v["name"] for v in data ] )
        
        if (self.tmin != None):
            keys = np.logical_and( self.timevalues > self.tmin , self.timevalues < self.tmax )
            self.timevalues = self.timevalues[keys]
            self.filelist = self.filelist[keys]
        
        self.reader = vtk.vtkXMLGenericDataObjectReader()
        self.ChangeTime( self.timevalues[0] )
            
    def ChangeTime(self,time):
        arg = np.min(np.where(self.timevalues == time))
        self.reader.SetFileName(  path.join(self.dirpath , self.filelist[arg] ) )
        self.curretTime = time
        self.reader.Update()
    
    def GetReader(self):
        return self.reader

    def GetDataSet(self):
        return self.reader.GetOutput()

class timeSeriesFoamReader(timeSeriesReader):        

    def __init__(self, label64 = False):
       
        self.label64 = label64
        timeSeriesReader.__init__(self)

    def Update(self):

        if ( path.splitext( self.filename )[1] != ".foam" ):
            self.dirpath = self.filename
            self.filename = path.join( self.dirpath, "mode.foam")

        with open( self.filename , "w") as f:
             f.write("")
             f.close()
                
        self.reader = vtk.vtkPOpenFOAMReader()
        self.reader.SetFileName( self.filename )
        self.reader.SetCaseType( self.reader.DECOMPOSED_CASE )
        if self.label64: self.reader.SetUse64BitLabels(True)
        self.reader.SetListTimeStepsByControlDict(False)
        self.reader.UpdateInformation()

        outinfo = self.reader.GetExecutive().GetOutputInformation(0)
        self.timevalues = np.array( outinfo.Get(vtk.vtkStreamingDemandDrivenPipeline.TIME_STEPS()) )
        
        if (self.tmin != None):
            keys = np.logical_and( self.timevalues > self.tmin , self.timevalues < self.tmax )
            self.timevalues = self.timevalues[keys]

        self.ChangeTime( self.timevalues[0] )

    def ChangeTime(self,time):
        arg = np.min(np.where(self.timevalues == time))
        self.curretTime = time
        self.reader.UpdateTimeStep(time)
        self.reader.Update()
        return self.reader
    
    def GetDataSet(self):
        output = self.reader.GetOutput().GetBlock(0)
        if (output.__class__ == vtk.vtkMultiBlockDataSet):
              output = output.GetBlock(0)
        return output

    def __del__(self):
        remove(self.filename)

