#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Jul 22 12:19:32 2022

@author: diogo
"""

import numpy as np
import matplotlib as plt
import vtk
import os
import numpy as np
import matplotlib.pyplot as plt
from vtk.util.numpy_support import vtk_to_numpy, numpy_to_vtk
from matplotlib.tri import Triangulation
from matplotlib.collections import PolyCollection

c = vtk.vtkDummyController()
vtk.vtkMultiProcessController.SetGlobalController(c)

reader = vtk.vtkXMLUnstructuredGridReader()
reader.SetFileName("dmd_cylinder.vtu")
reader.Update()
grid = reader.GetOutput()

eigenvalues = np.fromfile("eigenvalues.dat", dtype = np.complex)
amplitude = np.fromfile("amplitudes.dat", dtype = np.complex)
frequencies = np.fromfile("frequencies.dat", dtype = np.complex)

numOfArrays = grid.GetCellData().GetNumberOfArrays()

argcomp,  = np.where( eigenvalues.imag != 0  )
argreal,  = np.where( eigenvalues.imag == 0  )

if (numOfArrays == eigenvalues.size * 2):
    modes = np.array( [ vtk_to_numpy( grid.GetCellData().GetArray(k))+1j*vtk_to_numpy( grid.GetCellData().GetArray(k+1)) for k in range(0,grid.GetCellData().GetNumberOfArrays(),2) ] )   
elif(numOfArrays == eigenvalues.size):
    argcomp = argcomp[::2]
    index = np.cumsum( [0] + ( np.isin( arg, argreal ) * 1 + np.isin( arg, argcomp ) * 2).tolist()[:-1] )
    modes = np.array( [ vtk_to_numpy( grid.GetCellData().GetArray(k)) + 1j*(0 if real else vtk_to_numpy( grid.GetCellData().GetArray(k+1)) ) for k, real in zip(index, np.isin( arg, argreal ) ) ] )
    amplitude[argcomp] = 2* amplitude[argcomp]
    arg = np.sort( np.concatenate( (argreal, argcomp) ) )
    amplitude = amplitude[arg]
    eigenvalues = eigenvalues[arg]
else:
    raise ValueError("Número de Arrays no VTK é inválido")

modes = modes * amplitude.reshape( amplitude.size , 1 , 1 )

timeArray = np.arange(0,50,1)

dmdevol = grid.NewInstance()
dmdevol.CopyStructure(grid)

writer = vtk.vtkXMLUnstructuredGridWriter()
writer.SetNumberOfTimeSteps( timeArray.size)
writer.SetInputData(dmdevol)
writer.SetFileName("time.vtu")
writer.Start()

for t in timeArray:    
    test =  numpy_to_vtk(  np.sum( (  (eigenvalues)**t ).reshape( eigenvalues.size , 1,1) * modes, axis = 0 ).real )
    arrayId = dmdevol.GetCellData().AddArray(test)
    dmdevol.GetCellData().GetArray(arrayId).SetName("U")
    writer.WriteNextTime(t)
    dmdevol.GetCellData().RemoveArray(arrayId)    
    dmdevol.GetCellData().Update()
    
writer.Stop()

