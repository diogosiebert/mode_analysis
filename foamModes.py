#!/usr/bin/env python3.8
# -*- coding: utf-8 -*-

from argparse import ArgumentParser
from os import path, remove
import vtk
import math
from vtk.util.numpy_support import vtk_to_numpy , numpy_to_vtk
from vtk.numpy_interface import dataset_adapter as dsa
from timeSeries import timeSeriesJSONReader, timeSeriesFoamReader
from yaml import load, dump, Loader
import pickle

#Read script basic arguments
parser = ArgumentParser(description="Perform DMD analysis on a snapshot sequence.")
parser.add_argument('-i' , action="store" ,dest="yamlfile" ,  type = str, help="YAML file with all the parameters for the DMD analysis.", metavar='filename', default = None)
args = parser.parse_args()

f = open(args.yamlfile)
yaml = load( f.read() , Loader = Loader )
f.close()

import pydmd
import numpy as np
import pandas as pd
import statistics
import os

c = vtk.vtkDummyController()
vtk.vtkMultiProcessController.SetGlobalController(c)

{'output': {'modeinfo': {'filename': 'frequency.csv'}, 'modefield': {'filename': 'teste.vtu', 'real': True, 'imaginary': True, 'absolute': True, 'phase': True}}, 'input': {'type': 'foam', 'path': '.', 'foam64bits': False}, 'fields': ['U', 'p'], 'data': 'cell', 'time': {'range': {'min': 0, 'max': 0}, 'stride': 1}, 'region': {'x': {'min': 0, 'max': 0}, 'y': {'min': 0, 'max': 0}, 'z': {'min': 0, 'max': 0}}, 'dmd': {'svd_rank': 0, 'tlsq_rank': 0, 'sort': 'abs', 'exact': False, 'opt': False, 'rescale': False, 'forwardBackward': False}}

if ( yaml['input']['type'] == 'vtmseries'):
    timeseries = timeSeriesJSONReader()
elif ( yaml['input']['type'] == 'foam'):
    timeseries = timeSeriesFoamReader( label64 = yaml['input']['foam64bits'] )
else:
    raise ValueError("Invalid value ({}) for {}".format(yaml['input']['type'],"input:type") )

timeseries.SetFileName( yaml['input']['path'] )
timeseries.SetTimeInterval( *yaml['time']['range'] )
timeseries.Update()
timevalues = timeseries.GetTimeValues()
selectedTimes = timevalues[::yaml["time"]["stride"] ]

#  Actually read the data
reader = timeseries.GetReader()
baseGrid = timeseries.GetDataSet()
attributeIndex = 0 if yaml["data"] == "point" else 1
AttributeData = baseGrid.GetAttributes(attributeIndex)

#Exctract the field names found

if yaml["fields"] == None:
    arrayInfo = { AttributeData.GetArrayName(n) : (n, AttributeData.GetArray(n).GetNumberOfComponents()) for n in range(AttributeData.GetNumberOfArrays()) }
else:
    arrayInfo = { AttributeData.GetArrayName(n) : (n, AttributeData.GetArray(n).GetNumberOfComponents()) for n in range(AttributeData.GetNumberOfArrays()) if AttributeData.GetArrayName(n) in yaml["fields"] }

region = yaml["region"]["x"] + yaml["region"]["y"] + yaml["region"]["z"]

if ( region != [] ):
    box = vtk.vtkBox()
    box.SetBounds( *region )
    extract = vtk.vtkExtractGeometry()
    extract.SetInputData( baseGrid )
    extract.SetImplicitFunction(box)
    extract.ExtractInsideOn();
    extract.ExtractBoundaryCellsOn();
    extract.Update()
    grid = extract.GetOutput()
else:
    grid = baseGrid

numberOfCells =  grid.GetNumberOfPoints() if yaml["data"] == "point" else grid.GetNumberOfCells()
Xraw = np.empty( [ sum([ c for (n,c) in arrayInfo.values() ])*numberOfCells  , len( selectedTimes ) ]  )

dt = np.diff( selectedTimes )[0]

for n,time in enumerate( selectedTimes  ):

    print("Reading Time {:4f} :... ".format(time))

    timeseries.ChangeTime(time)

    if ( region == [] ):
        grid = timeseries.GetDataSet()
    else:
        extract.UpdateTimeStep(time)
        extract.SetInputData(  timeseries.GetDataSet()  )
        extract.Update()
        grid = extract.GetOutput()

    beg = 0
    for k, c in arrayInfo.values():
        end = beg + c*numberOfCells
        Xraw[beg:end,n] = vtk_to_numpy( grid.GetAttributes(attributeIndex).GetArray(k) ).flatten()
        beg = end

mode =  grid.NewInstance()
mode.CopyStructure( grid )
modeAttributeData = mode.GetAttributes(attributeIndex)

if yaml['method'] == 'dmd':

    import pydmd

    if yaml['dmd']['svd_rank'] > len( selectedTimes ):
            print("SVD rank is too large. Setting SVD rank to default value.")
            yaml['dmd']['svd_rank']=0

    if yaml['dmd']['tlsq_rank'] >len( selectedTimes ):
            print("TLSQ rank is too large. Not computing any noise reduction.")
            yaml['dmd']['tlsq_rank']=0

    yaml['dmd']['sorted_eigs'] = yaml['dmd']['sorted_eigs'] if (yaml['dmd']['sorted_eigs'] != "amp") else False
    dmd = pydmd.DMD( **yaml['dmd'] )
    dmd.fit(Xraw)

    print("{0} modes were found".format(len(dmd.eigs) ))
    
    if (yaml['dmd']['sorted_eigs'] == "amp"):
        argsort = np.flip( np.argsort( abs(dmd._compute_amplitudes()) )  )
    else:
        argsort = range(dmd.eigs.size)

    modes = dmd.modes.T[argsort].T
    eigenvalues = dmd.eigs[argsort]
    eps = 10*np.finfo( dmd.eigs.dtype ).resolution
    argreal, = np.where(eigenvalues.imag == 0)
    argcomp, = np.where(eigenvalues.imag != 0)
    argcomp = argcomp[::2]

    for n in np.sort( np.concatenate( (argreal, argcomp) ) ):

        eig = eigenvalues[n]
        beg = 0
        for name,(k, c) in zip(arrayInfo,arrayInfo.values()):

            end = beg + c*numberOfCells

            for fmt,func in ( ("real" , np.real), ("modulus" , np.abs) ) +  (() if n in argreal else ( ("imaginary" , np.imag),  ("argument" , np.angle) ) ) :
                if (fmt in yaml['output']['parts']):
                    dataArray = numpy_to_vtk( np.copy( func( modes[beg:end,n].reshape( numberOfCells,  c)) ) )
                    dataArray.SetName("{0} ({1:.3f}+{2:.3f}i) {3}{4}".format(str(n).zfill( math.ceil( math.log10( len(eigenvalues) ) ) ) ,eig.real,eig.imag,name,fmt[0]) )
                    modeAttributeData.AddArray(dataArray)

            beg = end

    df = pd.DataFrame()
    df["eigenvalue"] = eigenvalues
    df["frequency"] = np.log(eigenvalues)/dt
    df["amplitude"] = dmd._compute_amplitudes()[argsort]
    df.to_csv(yaml['output']['csvfile'])

if (yaml['method'] == "spod"):
    
    if yaml['spod']['implementation'] == 'low_storage':
        from pyspod.spod_low_storage import SPOD_low_storage  as spodImplementation
    elif yaml['spod']['implementation'] == 'low_ram':
        from pyspod.spod_low_ram     import SPOD_low_ram as spodImplementation
    elif yaml['spod']['implementation'] == 'streaming':
        from pyspod.spod_streaming   import SPOD_streaming as spodImplementation
    
    params = yaml['spod']
    params['time_step'   ] = dt # data time-sampling
    params['n_snapshots' ] = Xraw.shape[1]    # number of time snapshots (we consider all data)
    params['n_space_dims'] = 1              # number of spatial dimensions (longitude and latitude)
    params['n_variables' ] = 1              # number of variables
    
    SPOD_analysis = spodImplementation(data=Xraw.T, params=params, data_handler=False, variables=("phi") )
    spod = SPOD_analysis.fit()
        
    for freq in range(SPOD_analysis._n_freq):
        for n in range(SPOD_analysis._n_modes_save):
            beg = 0

            for name,(k, c) in zip(arrayInfo,arrayInfo.values()):
        
                end = beg + c*numberOfCells

                for fmt,func in (("real" , np.real),  ("modulus",np.abs)  ,  ("imaginary",np.imag),  ("argument",np.angle)):
                    if (fmt in yaml['output']['parts']):            
                        dataArray = numpy_to_vtk( np.copy( func( np.squeeze(SPOD_analysis.get_modes_at_freq(freq).T[n])[beg:end].reshape( numberOfCells,  c)) ) )       
                        dataArray.SetName("{0} freq_{1}_mode_{2}_{3}".format(name,freq,n,fmt[0])) 
                        modeAttributeData.AddArray(dataArray)
                
                beg = end

writer = vtk.vtkXMLDataSetWriter()
writer.SetInputData(  mode )
writer.SetFileName( yaml['output']['vtkfile'] )
writer.SetDataModeToBinary()
writer.SetCompressorTypeToZLib()

try:
	writer.SetCompressionLevel(4)
except:
	pass
writer.Update()
writer.Write()

#if yaml['output']['picklefile'] != False and yaml['output']['picklefile'] != None:
    #cellCentersFilter=vtk.vtkCellCenters()
    #cellCentersFilter.SetInputData(grid)
    #cellCentersFilter.VertexCellsOff()
    #cellCentersFilter.Update()
    #cellCenters=dsa.WrapDataObject(cellCentersFilter.GetOutput()).Points
    #cellCenters=cellCenters.reshape(numberOfCells,3)
    #with open(args.pickleoutput,'wb') as f:
        #pickle.dump(dmd,f) 
        #pickle.dump(cellCenters,f)
        #f.close()
